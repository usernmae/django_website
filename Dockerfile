FROM python:3.7-alpine

WORKDIR /backend

RUN apk add --update --no-cache postgresql-dev gcc python3-dev musl-dev gcc libpq

RUN pip install pipenv psycopg2-binary

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install --system --deploy

COPY . .
RUN python ./manage.py collectstatic --noinput

CMD python ./manage.py migrate & python ./manage.py runserver 0.0.0.0:7777