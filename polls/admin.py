from django.contrib import admin
from polls.models import Poll, Choice


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3


def remove_choices(modeladmin, request, queryset):
    Choice.objects.filter(poll__in=queryset).delete()


remove_choices.short_description = "Remove all choices from poll"


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline]
    actions = [remove_choices]
