from django import forms


class LoginForm(forms.Form):
    login = forms.CharField(label="Login", required=True, widget=forms.TextInput(),)
    password = forms.CharField(
        label="Password", required=True, widget=forms.PasswordInput(),
    )
