from django.contrib.auth import login, authenticate, logout
from django.core.exceptions import PermissionDenied
from django.core.handlers.wsgi import WSGIRequest
from django.core.paginator import Paginator
from django.shortcuts import redirect
from django.views import View
from django.views.generic import TemplateView

from polls.forms import LoginForm
from polls.models import Poll, Choice


class PollsView(TemplateView):
    template_name = "polls.html"

    def get(self, request: WSGIRequest, *args, **kwargs):
        objects = Poll.objects.prefetch_related("choice_set").all()
        p = Paginator(objects, 10)
        page = request.GET.get('page', 1)
        context = self.get_context_data(polls=Poll.objects.prefetch_related('choice_set').all())
        return self.render_to_response(context)


class ChoiceView(View):
    def get(self, request: WSGIRequest, poll_id: int, choice_id: int, *args, **kwargs):
        choice = Choice.objects.get(id=choice_id, poll=poll_id)
        choice.votes += 1
        choice.save()
        return redirect(f"/polls/")


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request: WSGIRequest, *args, **kwargs):
        context = self.get_context_data(form=LoginForm())
        return self.render_to_response(context)

    def post(self, request: WSGIRequest, *args, **kwargs):
        data = LoginForm(request.POST)

        if data.is_valid():
            user = authenticate(
                request,
                username=data.cleaned_data['login'],
                password=data.cleaned_data['password']
            )

            if user:
                login(request, user)
                return redirect("/polls/")

        raise PermissionDenied()


class LogoutView(View):
    def get(self, request: WSGIRequest, *args, **kwargs):
        logout(request)
        return redirect("/polls/login")
