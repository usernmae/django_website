from django.contrib.auth.models import User
from rest_framework import serializers

from polls.models import Choice


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ['poll', 'choice_text', 'votes']
