from rest_framework import routers

from rest.views import UserViewSet, ChoiceViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'choice', ChoiceViewSet)

urlpatterns = router.urls
