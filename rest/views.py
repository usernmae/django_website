from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework import viewsets
from polls.models import Choice

from rest.serializers import UserSerializer, ChoiceSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer
