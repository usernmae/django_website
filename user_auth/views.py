from django.core.handlers.wsgi import WSGIRequest
from django.views.generic import TemplateView

from polls.forms import LoginForm


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request: WSGIRequest, *args, **kwargs):
        context = self.get_context_data(form=LoginView())
        return self.render_to_response(context)

    def post(self, request: WSGIRequest, *args, **kwargs):
        data = LoginForm(request.POST)
